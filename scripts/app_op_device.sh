#!/bin/sh

OUTPUT_TAINT=/data/data/org.appanalysis/cache
OUTPUT_VPN=/data/data/com.y59song.LocationGuard/cache
LOCALOUTPUT_TAINT=../output/taint
LOCALOUTPUT_VPN=../output/vpn
RECORD_OUTPUT=../record
RERAN_DIR=../RERAN
GOOGLEPLAY_DIR=../related/google-playservices

# Usage : get_app.sh device option applist [number [path]]
path="../download"
device=${1}
option=${2}
number=${4}
counter=0

uninstallApp () {
  file=$1
  adb -s $device uninstall $file
  echo "Uninstall Done"
}

installApp () {
  file=$1
  adb -s $device install "$path/$file.apk"
  echo "Install Done"
}

setupDevice () {
  device=$1
  adb -s $device push "${RERAN_DIR}/replay" /data/local

  adb -s $device shell mount -o remount r,w /system
  adb -s $device shell chmod 777 /system/app
  adb -s $device push "${GOOGLEPLAY_DIR}/GoogleLoginService.apk" /system/app/.
  adb -s $device push "${GOOGLEPLAY_DIR}/GoogleServicesFramework.apk" /system/app/.
  adb -s $device push "${GOOGLEPLAY_DIR}/Vending.apk" /system/app/.
  # adb -s $device push "${GOOGLEPLAY_DIR}/NetworkLocation.apk" /system/app/.
  # adb -s $device install "${GOOGLEPLAY_DIR}/Gms.apk"
  adb -s $device shell rm /system/app/SdkSetup*
}

setup () {
  setupDevice $device

  echo "Setup Done"
}

testApp () {
  file=$1

  adb -s $device shell rm "${OUTPUT_DEVICE}/${file}"

  echo "Start test"
  adb -s $device shell getevent -tt > "${RECORD_OUTPUT}/${file}_device_origin" &

  trap 'kill $(jobs -p) && uninstallApp $file' EXIT
}

getData() {
  file=$1
}

reran () {
  file=$1
  java "${RERAN_DIR}/translate" "${RECORD_OUTPUT}/${file}_device_origin" > "${RECORD_OUTPUT}/${file}_device"

  adb -s $device push "${RECORD_OUTPUT}/${file}_device" /data/local
  adb -s $device shell /data/local/replay "/data/local/${file}_device"
}

if [ $option = "setup" ]; then
  setup
  exit
fi

while read file
do
  if [ ${#file} -lt 2 ]; then
    continue
  fi

  if [ ${file:0:1} = "#" ]; then
    continue
  fi

  counter=$(($counter+1));
  if [ $counter -ne $number ]; then
    continue
  fi

  if [ $option = "test" ]; then
    testApp $file
  elif [ $option = "getData" ]; then
    getData $file
  elif [ $option = "installTest" ]; then
    uninstallApp $file
    if [ ! -f "$path/$file.apk" ]; then
      echo "$file not exists"
    fi
    installApp $file
    testApp $file
  elif [ $option = "reran" ]; then
    uninstallApp $file
    if [ ! -f "$path/$file.apk" ]; then
      echo "$file not exists"
    fi
    installApp $file
    reran $file
  elif [ $option = "install" ]; then
    if [ ! -f "$path/$file.apk" ]; then
      echo "$file not exists"
    fi
    uninstallApp $file
    installApp $file
  elif [ $option = "uninstall" ]; then
    uninstallApp $file
  else
    echo "undefined operation"
    exit 1
  fi
done < $3
