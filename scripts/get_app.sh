#!/bin/sh
# get_app.sh applist [number [path]]
path="download"
number=500

if [ ${#} -gt 1 ]; then
  number=${2}
fi

if [ ${#} -gt 2 ]; then
  path=${3}
fi

while read file
do
  if [ ${#file} -lt 2 ]; then
    continue
  fi

  if [ ${file:0:1} = "#" ]; then
    continue
  fi

  if [ ! -f "../$path/$file.apk" ]; then
    echo "$file"
    python ./googleplay-api/download.py "$file" "../${path}/${file}.apk"
  else
    echo "${file} downloaded"
  fi
  counter=$(($counter+1));
  if [ $counter -eq $number ]; then
    exit 0;
  fi
done < $1
