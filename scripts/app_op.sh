#!/bin/sh

OUTPUT_TAINT=/data/data/org.appanalysis/cache
OUTPUT_VPN=/data/data/com.y59song.LocationGuard/cache
LOCALOUTPUT_TAINT=../output/taint
LOCALOUTPUT_VPN=../output/vpn
RECORD_OUTPUT=../record
RERAN_DIR=../RERAN
GOOGLEPLAY_DIR=../related/google-playservices

# Usage : get_app.sh option applist [number [path]]
path="../download"
option=${1}
number=${3}
counter=0

device1="emulator-5554" # taint droid
device2="emulator-5556" # android

uninstallApp () {
  file=$1
  adb -s $device1 uninstall $file
  adb -s $device2 uninstall $file
  echo "Uninstall Done"
}

installApp () {
  file=$1
  adb -s $device1 install "$path/$file.apk"
  adb -s $device2 install "$path/$file.apk"
  echo "Install Done"
}

setupDevice () {
  device=$1
  adb -s $device push "${RERAN_DIR}/replay" /data/local

  adb -s $device shell mount -o remount r,w /system
  adb -s $device shell chmod 777 /system/app
  adb -s $device push "${GOOGLEPLAY_DIR}/GoogleLoginService.apk" /system/app/.
  adb -s $device push "${GOOGLEPLAY_DIR}/GoogleServicesFramework.apk" /system/app/.
  adb -s $device push "${GOOGLEPLAY_DIR}/Vending.apk" /system/app/.
  # adb -s $device push "${GOOGLEPLAY_DIR}/NetworkLocation.apk" /system/app/.
  # adb -s $device install "${GOOGLEPLAY_DIR}/Gms.apk"
  adb -s $device shell rm /system/app/SdkSetup*
}

setup () {
  setupDevice $device1
  setupDevice $device2

  echo "Setup Done"
}

testApp () {
  file=$1

  adb -s $device1 shell rm "${OUTPUT_TAINT}/${file}"
  adb -s $device2 shell rm "${OUTPUT_VPN}/${file}"

  echo "Start test"
  adb -s $device1 shell getevent -tt > "${RECORD_OUTPUT}/${file}_taint_origin" & adb -s $device2 shell getevent -tt > "${RECORD_OUTPUT}/${file}_stock_origin"

  trap 'kill $(jobs -p) && uninstallApp $file' EXIT
}

getData() {
  file=$1
  adb -s $device1 pull "${OUTPUT_TAINT}/${file}" "${LOCALOUTPUT_TAINT}/${file}_temp"
  adb -s $device2 pull "${OUTPUT_VPN}/${file}" "${LOCALOUTPUT_VPN}/${file}_temp"

  if [ ! -f "${LOCALOUTPUT_TAINT}/${file}" ]; then
    touch "${LOCALOUTPUT_TAINT}/${file}"
  fi
  if [ ! -f "${LOCALOUTPUT_VPN}/${file}" ]; then
    touch "${LOCALOUTPUT_VPN}/${file}"
  fi

  cat "${LOCALOUTPUT_TAINT}/${file}_temp" >> "${LOCALOUTPUT_TAINT}/${file}"
  cat "${LOCALOUTPUT_VPN}/${file}_temp" >> "${LOCALOUTPUT_VPN}/${file}"

  adb -s $device1 shell rm "${OUTPUT_TAINT}/${file}"
  adb -s $device2 shell rm "${OUTPUT_VPN}/${file}"

  rm "${LOCALOUTPUT_TAINT}/${file}_temp"
  rm "${LOCALOUTPUT_VPN}/${file}_temp"
}

reran () {
  file=$1
  adb -s $device1 push "${RECORD_OUTPUT}/${file}_taint" /data/local
  adb -s $device1 push "${RECORD_OUTPUT}/${file}_stock" /data/local
  adb -s $device1 shell /data/local/replay "/data/local/${file}_taint"
  adb -s $device2 shell /data/local/replay "/data/local/${file}_stock"

  java "${RERAN_DIR}/translate" "${RECORD_OUTPUT}/${file}_taint_origin" > "${RECORD_OUTPUT}/${file}_taint"
  java "${RERAN_DIR}/translate" "${RECORD_OUTPUT}/${file}_stock_origin" > "${RECORD_OUTPUT}/${file}_stock"
}

if [ $option = "setup" ]; then
  setup
  exit
fi

while read file
do
  if [ ${#file} -lt 2 ]; then
    continue
  fi

  if [ ${file:0:1} = "#" ]; then
    continue
  fi

  counter=$(($counter+1));
  if [ $counter -ne $number ]; then
    continue
  fi

  if [ $option = "test" ]; then
    testApp $file
  elif [ $option = "getData" ]; then
    getData $file
  elif [ $option = "installTest" ]; then
    uninstallApp $file
    if [ ! -f "$path/$file.apk" ]; then
      echo "$file not exists"
    fi
    installApp $file
    testApp $file
  elif [ $option = "reran" ]; then
    uninstallApp $file
    if [ ! -f "$path/$file.apk" ]; then
      echo "$file not exists"
    fi
    installApp $file
    reran $file
  elif [ $option = "install" ]; then
    if [ ! -f "$path/$file.apk" ]; then
      echo "$file not exists"
    fi
    uninstallApp $file
    installApp $file
  elif [ $option = "uninstall" ]; then
    uninstallApp $file
  else
    echo "undefined operation"
    exit 1
  fi
done < $2
