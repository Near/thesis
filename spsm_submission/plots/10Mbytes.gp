set terminal pdf enhanced font "Helvetica,8"
set output '10Mbytes.pdf'

set style data histogram 
#set style histogram cluster gap 1
set style histogram errorbars gap 2 lw 1
set key at graph 0.97,1
set key font ",8"
set datafile separator ","
#set xlabel "Attributes Returned"
set ylabel "Delay (ms)"

set style fill solid border rgb "black"
set auto x
set yrange [0:7500]
#set ytics ("0%%" 0, "10%%" 10, "20%%" 20, "30%%" 30, "40%%" 40, "50%%" 50, "60%%" 60, "70%%" 70, "80%%" 80)
plot '10Mbytes.dat' using 2:3 title col, \
        '' using 4:5:xtic(1) title col, \
        '' using 6:7 title col, \
        '' using 8:9 title col
