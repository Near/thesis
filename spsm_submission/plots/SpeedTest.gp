set terminal pdf enhanced font "Helvetica,8"
set output 'SpeedTest.pdf'

set style data histogram 
#set style histogram cluster gap 1
set style histogram errorbars gap 2 lw 1
set key at graph 0.7,1
set key font ",8"
set datafile separator ","
#set xlabel "Attributes Returned"
set ylabel "Speed (Mbps)"
set y2label "Time (ms)"

set style fill solid border rgb "black"
set auto x
set yrange [0:40]
set y2range [0:40]
set ytics 10
set y2tics 10
#set ytics ("0%%" 0, "10%%" 10, "20%%" 20, "30%%" 30, "40%%" 40, "50%%" 50, "60%%" 60, "70%%" 70, "80%%" 80)
plot 'SpeedTest.dat' using 2:3 title col, \
        '' using 4:5:xtic(1) title col, \
        '' using 6:7 title col axes x1y2
