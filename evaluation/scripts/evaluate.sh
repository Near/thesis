#! /bin/bash

#if [ $# -ne 1 ] || [ $1 -lt 1 ] || [ $1 -gt 3 ]
#then
#  echo "Usage: $0 level[1-3]"
#  exit
#fi

apkDIR="/home/frank/Source/thesis/download"

packName=$1
fileName="${packName}.apk"

if [ ! -f "${apkDIR}/${fileName}" ]; then
    echo "Not downloaded"
    exit 0
fi

appName=$(aapt d badging "${apkDIR}/${fileName}" | grep "application: label=")
pattern="application: label='([^']*)'"
if [[ $appName =~ $pattern ]]; then
  appName=${BASH_REMATCH[1]};
fi
echo "$packName" > app.info
echo "$appName" >> app.info
cat app.info

adb install -r "${apkDIR}/${fileName}"

adb uninstall ${packName}
