Page 1:
  Hello everyone, thanks for coming to my master thesis presentation. Today I'm going to talk about PrivacyGuard, A VPN based approach to detect privacy leakages on Android devices. My name is Yihang Song and my supervisor is Urs.

Page 2:
  My talk consists of four parts. The first part describes why we want to develop PrivacyGuard. In the second part, I will briefly introduce the design and implementation of Privacy. We evaluated the effectiveness and efficiency of PrivacyGuard and results are shown in part three. I will conclude in the fourth part.

Page 3:
  So why do we want to detect privacy leakages on mobile devices? As every knows, mobile devices are more and more popular. They are embedded with a lot of sensors. There are some traditional types, like GPS, gyroscopes and accelerators. And some new ones, like fingerprint sensors on iPhones and heart rate sensors in wearable sensors. Like this one I currently wear, which is not quite accurate.

Page 4:
   With these sensors, mobile devices are much more powerful. That's good, right? Why does it matter?

Page 5:
  Well, It really matters because of this, this and this. We did a research in 2013 which is about how to infer keyboard input on Android devices by only taking advantages of accelerators data. With location, device ID and so on, attackers can track you, identify you or profile you. That's why we want to detect these privacy leakages on mobile devices.

Page 6:
  There have been some mechanisms on current platforms. This one is used on the iOS. It is the first-time use notification. This one is from Windows phone and shows at the first-time use. The third one is used on Android. It shows when users install an application and informs users what permissions the application requires.

Page 7:
  There are some obvious problems with these mechanisms. The first problem is, these are hard to understand for average users. Just imagine, what will users usually do when they see these notifications? They may hesitate for 1-2 seconds at the first time and then just click OK quickly later on. And all of them are all-or-nothing.

  To be more specific, in Android, there are several problems. Most users, like me, will just ignore the permissions prompt. If they are not comfortable with the permissions, they can only give up the installation.

Page 8:
  We come up with PrivacyGuard to address these problems. There are several we have in mind when designing it. It must be able to detect privacy leakages of most applications. Unlike those existing mechanisms, it must be easy to use and understand. Usability is important, therefore, it should not require root permissions or flashing. It must be portable since failure in upgrading Android is a big source of insecurity. It should only introduce acceptable overhead to allow people use it in daily life. At last, we hope PrivacyGuard is extensible. Developers can add their own functionalities and researchers can prototype their own algorithms and strategies.

Page 9:
  The main idea of PrivacyGuard is, intuitively, if nothing is sent out, nothing goes out from the device. There can't be any privacy leakages right? If we have access to all network traffic, we can detect privacy leakages by filtering.

Page 10:
  So, how do we gain the access? There are two possible ways. One is using a proxy. However, due to the restrictions of Android, no applications can configure the proxy programmatically and the configuration should be done for each connection. The other one, also our method, is using a VPN. A VPN server has access to all network traffic. If we let the device connect to the server, we get the access.

Page 11:
  Here comes the problem!

Page 12:
  Why should users trust the VPN server?
  What if the server is compromised?

Page 13:
  A remote server is not good enough. We use a fake VPN server runs locally. By fake, we mean that it is not a real VPN server. It has no other features, like encryption.

  The Android SDK provides a class called VPNService in android versions newer than Android 4.0. It creates a virtual network interface, configures addresses and routing rules, and returns a file descriptor to the application. The application can retrieve network traffic by reading the descriptor. With it, we design and implement PrivacyGuard.

Page 14-15:
  This is the architecture of PrivacyGuard. It has five main components. The fakeVPNService which extends VPNService, the tcpforwarder which implements a simple TCP protocol, the localserver which acts as a man-in-the-middle proxy, the socketforwarder which retransmits messages between applications and real servers and the plugins which do fiterings. The work flow is ... I will explain these components according to the workflow.

Page 16:
  So, the first component is FakeVPNService. It has several jobs. It configures routing rules to make sure packets sent to any ip address will be trasmitted to the virtual interface. It also set DNS server address and creates two threads : TunReadThread and TunWriteThread. Applications send requests to the virtual interface and receive responses from it. TunReadThread keeps reading requests from the interface and a dispatch thread will dispatch packets to appropriate forwarders. TunWriteThread keeps writing responses to the interface.

Page 17:
  Since TCP is a stateful protocol, TCPForwarders have to main the states of tcp connections. They need to handle the ack num, seq number and so on. For each tcp connection, there is a different tcpforwarder. We maintain a mapping relationship based on the port number.

  Each forwarder will create a worker thread for sending and receiving packets.

Page 18:
  The TCPForwarder will send requests to the Local server. The local server acts as a man-in-the-middle proxy. And it creates socket forwarders.

Page 19:
  This picture shows the basic idea for man-in-the-middle proxy. Notice that the middle part is in plain text. This is necessary since the filtering works on plain texts but SSL encrypts packets.

Page 20:
  So how does LocalServer work as a proxy? When privacy guard is launched at the first time, it will generate a root CA certificate and install it. For each SSL connection, localserver will first do host name resolving with the destination address by trying to establish a connection and acquiring the certificate. After this step, we know the subject name of the destination. Then it generates a certificate for the site with our root CA signing it, does the handshake with the application. Of course, plain sockets are replaced with sslsockets.

  Now, all messages read from or write to the sslsockets are in plain texts.

Page 21:
  Socket forwarders are created by LocalServer. It contains two sockets, the client socket and the target socket. and two threads. In one thread, the client socket retransmits messages between the TCPForwarder and the target socket. In another thread, the target socket retransmits messages between the client socket and the real server. Since all messages read from the sslsockets are in plain texts, messages between client and server are plain texts.

Page 22:
  The last component is plugins. They do analysis on messages between the client and target sockets. We have only implemented three plugins but it is easy to add more. The three plugins are location detection which detects location, phonestate detection which detection device identifiers and contact detection which detects contact information.

  Here, we provide two filtering options, one is synchonous filtering which filters before messages are sent to the target. The other one is asynchronous filtering which first sends the messages to the target and stores these to a queue. Another thread will do filtering asynchorously.

Page 23:
  For evaluation, we focus on two aspects. One is performance which includes battery consumption and network speed. the other is effectiveness. We want to see if privacy guard can detect privacy leakages effectively.

Page 24:
  For battery consumption, here is how we do the evaluation. First, we fully charge it to avoid potential noises. Then we start to upload and download a 1 MBytes file every 10 seconds for 1 hour. We will also turn off the screen.

  The table shows the result. We can see there is no observable difference in the power consumption between different settings even for this frequent downloading and uploading task.

Page 25:
  For network speed, we do three different experiments for four configurations. The four configurations are No privacyguard, privacyguard without filtering, privacyguard with sync filtering and with async filtering. Every experiment are done in 10 rounds. For each round, we test all four configurations to avoid the unstability of WiFi network.

  The first experiment is uploading and downloading a 1Mbytes file.
  The graph shows the average result and standard deviation. The y axis means the delay of finishing the task. There is no result for async filtering in downloading because it only affects uploading. The table shows the accurate number. For downloading, there is only little overhead. For uploading, the average overhead is about 30 percent.

Page 26:
  For downloading and uploading 10MBytes files, we can see that the overhead for downloading is still small. For uploading, the average overhead is about 10 percent, which is much lower than that for 1Mb files.

Page 27:
  The third experiment is using the SpeedTest.net Applcation. It will test the ping delay, downloading and uploading speed. For downloading, the overhead is still very small. PrivacyGuard even has a higher download speed with sync filtering. For ping, although the percentage is high, the value is not very high. The ping delays are about 25 ms. For the uploading part, there is 22.19% overhead without filtering and 30.22% overhead with sync filtering. From the original data, we actually observe that PrivacyGuard can achieve a similar download speed with no privacy guard. But there are a lot of EPIPE errors which slow down the speed in some tests. If we can fix these issues, we think we can improve the uploading speed a lot.

Page 28:
  From all these results, although there is overhead in uploading, this large-content and frequent uploading scenario is rare in daily use. Usually the outgoing packets are short HTTP requests. We don’t think it will affect the usability of PrivacyGuard too much.

Page 29:
  For the effectiveness, we used 53 applications. 48 of them are used in other two papers. The 5 left are dummy applications written by ourselves which wrap popular advertisement libraries, like Admob from google.

Page 30:
  The evalution consists of two phases: training and testing.

Page 31:
  While doing the evaluation, we followed these steps:
  Uninstall the application if already installed and then install it
  Run simpleLocation, which gets data from all available location providers, to make sure getLastKnownLocation() returns a valid value to the evaluated application
  Manually deep crawl the application

Page 32:
  In the traning phase, we use the output of TaintDroid as the ground truth and derive filters for several types of sensivit information.

Page 33:
  Here are the filters we derived. For location, we use the data from all location providers and keep two decimal points of the longitude and lattitude as filters. For device identifiers, we detect IMEI, IMSI and android id. We not detect the plain text but also see if the traffic contains the SHA or MD5 hash of the original data. For contact, we only look for the original data of phone numbers and email addresses.

Page 34:
  Here is the result. We are able to derive filters for almost all applications. The reason we cannot detect Twitter is that Twitter uses SSL pinning.

Page 35:
  In the testing phase, we test the other 43 applications. PrivacyGuard detect 5 more applications than TaintDroid for location, 9 more for device ID. For every application PrivacyGuard reported, we manually checked the network traffic and we were able to find sensitive information.

Page 36:
  From the result, we conclude that PrivacyGuard can detect privacy leakages of most applications effectively.

Page 37:
  In conclusion. We design and implment PrivacyGuard. It requires no root permissions, introduces acceptabe overhead and is able to detect privacy leakages effectively. Also, it supports ssl traffic analysis.

Page 38:
  That's all. Thanks! Any questions?
