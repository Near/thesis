#!/usr/bin/python

import csv
import sys
import pandas as pd
import matplotlib.pyplot as plt

data = {}

def parse(experiment):
  if len(experiment) <= 0:
    return
  sub_title = experiment[0][0]
  num = len(experiment[0]) - 2
  for row in experiment[1:]:
    if sub_title not in data.keys():
      data[sub_title] = {}
      data[sub_title][row[0]] = map(float, row[1:num + 1])

def draw(data):
  exp = []
  res = {}
  for key1 in data.keys():
    exp += [key1] * 10
    if res == {}:
      res = data[key1]
    else:
      for key in res:
        res[key] += data[key1][key]
  ix = pd.MultiIndex.from_arrays([exp], names=['experiment'])
  dx = pd.DataFrame(res, index=ix)
  gp = dx.groupby(level=('experiment'))
  means = gp.mean()
  errors = gp.std()
  fig, ax = plt.subplots()
  means.plot(yerr=errors, ax=ax, kind='bar')

def drawFromFile(inputFile):
  with open(inputFile, 'rb') as csvFile:
    reader = csv.reader(csvFile, delimiter = ',')
    experiment = []
    for row in reader:
      if row[0] == '':
        parse(experiment)
        experiment = []
      elif row[1] == '':
        main_title = row[0]
      else:
        experiment.append(row)
  draw(data)
