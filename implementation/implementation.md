# Implementation

## Basic Ideas
The main idea is that we need to find a way such that we can acquire the access to all network traffic. With this access, we can do analysis and figure out if there is any leakage of private information. It works like a tcpdump or wireshark. Since Android is based on Linux, porting tcpdump seems feasible (and in fact, there is a tcpdump of Android version), but it requires root privilege, which is unacceptable to us.

We found two possible ways to achieve this goal. One is using proxy and the other is VPN. Because of the restrictions set by Android, no app can configure the proxy automatically without root permission and configuring it may be a problem to average users. We choose the second way, VPN.

### What is VPN
VPN refers to virtual private network. It extends a private network across a public network such as Internet. It is created by establishing a virtual point-to-point connection through the use of dedicated connections. VPN works on the IP layer.

### Possible options
Android SDK provides a very convenient way to connect to a VPN server programmatically. The user just needs to click OK and everything is set.

#### 1. Remote VPN server
The most easy way is that we can setup a real VPN server, let the device connect to it. With the connection, all network traffic will go through the server. The server can do the analysis and then report all leakages to users. This option is easy to setup and use but it has some problems.
* The network traffic will be sent to a third party server which users may not trust.
* High load for the server if there are many users.
* Network performance.

To avoid these problems, we came up with the second option

#### 2. Local VPN server
Since we don't want to use a remote server, intuitively we can implement one and run it locally. Then all network traffic can only be available to the device and the authentic server users want to access. It seems good but it has the following problem.
* The VPN server needs to run on the IP layer, but we cannot use raw socket (which receive ip packets) without root permission.

Again, root the device is not what we want. Besides, do we really need a VPN server? We need the access to all traffic, but not all other features of VPN.

#### 3. Fake VPN server
We can use the VPNService provided by Android SDK and pretend that we have connected to a server. However, we only get the traffic from the virtual network interface provided by VPNService and retransmit those after the analysis. I will explain the details in later sections.

### Main difficulties
* Because VPN works on IP layer, we have to parse the IP layer and reimplement a simple TCP/UDP protocol.
* All analysis only work on plain text which means we have to decrypt the packets using SSL.


## Framework
![framework](pics/architecture.png)

From the picture, $APP has 4 parts.
* VPN Service
* TCP Forwarder (or UDP Forwarder)
* SocketForwarder
* Local Server

Because almost all parts except the forwarding are similar between TCP and UDP, and the forwarding of TCP are much more important and complex, all things are based on the TCP unless explicitly specified.

### Basic work flow
* An app sends a request to a server, it will then be available from the virtual interface in MyVPNServie.
* MyVPNService will then parse the request which is in IP datagram format and dispatch it to the corresponding forwarder. In TCP case, the TCPForwarder will handle it.
* TCPForwarder implements TCP protocol and communicate with local server. Local server acts like a man-in-the-middle attacker.
* In local server, for each request from TCPForwarder, it will retransmit to the authentic server and also retransmit the response from the server to the TCPForwarder.
* TCPForwarder will package the response from local server into a IP datagram will send to the app.

In the following sections, I will explain all parts in details.

### VPN Service
#### Android VPN Service
The class [`VpnService`](http://developer.android.com/reference/android/net/VpnService.html) is added to Android SDK after API level 14 (Android 4.0). It creates a virtual network interface, configures addresses and routing rules, and returns a file descriptor to the application.

#### MyVpnService
Establishing an interface with address `10.8.0.1` and set routing rules to routing network traffic to all ip addresses. Besides, I setup several threads.
* TunReadThread, read requests from the interface and add it to a queue.
  * This thread will create a dispatch thread which will retrieve requests from the queue and dispatch to corresponding forwarders.
* TunWriteThread, retrieve responses from a queue and write these to the interface. The responses are added to the queue by forwarders by calling `write()` methods defined in `TunWriteThread` class.

Notice that these two queues are access and modified by more than two threads which means we need to consider the concurrent issues.

##### Dispatch thread
Since the request we get from the interface is IP datagram, we have to first retrieve the protocol information. If it contains a tcp packet, we will find a TCPForwarder by looking up the port number to handle this. If there has been a forwarder for the port number, just use it. Else we will create a new forwarder.

The reason we want to use the old one is because that TCP protocol has status.

##### IP datagram reconstruction
We need to reconstruct a valid IP datagram after we receive a TCP response. The main part is reverse the source and destination IP addresses in the request and update the IP checksum.

### TCPForwarder
#### TCP protocol
TCP protocol is designed to provide a reliable, ordered and error-checked delivery of a stream of octets between programs. It works on trasport layer and provides a end-to-end connection.

A tcp connection mainly has three states:
1. Connection establishment ([pic source](http://www.tcpipguide.com/free/t_TCPConnectionEstablishmentProcessTheThreeWayHandsh-3.htm))
![Connection establishment](pics/tcpopen3way.png)
2. Data transmission
3. Connection termination ([pic source](http://www.tcpipguide.com/free/t_TCPConnectionTermination-2.htm))
![Connection termination](pics/tcpclose.png)

To achieve all goals of TCP protocol, it has status. The sequence number and acknowledge number matters and it won't work if these are messed up.

#### My implementation
To store the connection status required by TCP protocol, we have to maintain a one to one mapping relationship between one TCPForwarder and one connection. Since the connection can be determined by the ip address (which is same for all apps) and port number, we only need to consider the port number.

##### Connection with Local Server
Briefly, TCPForwarder communicates with local server. The messages transferred between these two are requests from MyVpnService and responses from real servers.

TCPForwarder connects to local server just like connecting to a normal server. It use the local host and the source port number in the tcp packet to create a socket and connects to local server (ip address and port number are determined values) with the socket.

##### TCPForwarder to LocalServer
This direction is easy. What we only need to is retrieving the data and then sending it.

##### LocalServer to TCPForwarder
For this direction, we need to create a tcp packet by using the data and the status we maintain. The tcp checksum recalculation is also necessary.

##### Connection status
The tcp connection status is stored in `TCPConnectionInfo` and it implements all necessary methods to update the status.

##### Non-block transmission
Because the TCP protocol use stream and a request may be in different packets, using block transmission will be very complex and introduce more performance overhead. In my implementation, I use two threads. One sends the requests to local server and one reads responses from local server when it is readable.

##### TCP checksum recalculation
TCP checksum is used to provide the error-checked feature. The checksum calculation requires all tcp information and part of ip header.

### Local Server
From the point view of apps, local server acts as the authentic server. Physically, apps only communicate with the local server. It listens on a specific port which is known and connected by all TCPForwarders.

#### Connection accepted
Whenever a "client" (in our case, `TCPForwarder`) connects to it, local server will create a pair of two sockets, `client` and `target`.

`client` is similar with the socket created by a normal TCP server when accepting a connection. `target` is one socket which connects to the real server.

#### How to determine the real server
Remember that the socket used by TCPForwarder to connect to local server use the same port number with the app (but ip different address), we can use the port number to figure out which IP address and port number the app actually wants to connect to. This information can be found `/etc/tcp` or `/etc/tcp6`.

#### Handle SSL connection <a name="handle_ssl_connection"></a>
As mentioned before, all analysis can only be done on plain texts. However, ssl will encrypt the data. To reveal these data, we need to execute a man in the middle attack.

##### Man in the middle attack

##### Implementation
We create a root CA and certificates for each websites visited by apps. Since local server acts as a real server in the view of apps, when an app want to establish a ssl connection, local server will send the corresponding certificate and both `client` and `target` will also be a `SSLSocket`. Although the traffic between apps and `client` is encrypted or between `target` and Internet is encrypted, we can read data from `client` and `target` in plain text.

### SocketForwarder
Socket forwarders match one to one with tcp connections. Each socket forwarder contains the two sockets created by local server for one particular connection.

* `client` sends data from `TCPForwarder` to `target` which will then retransmit to the authentic server.
* `target` sends respones from the authentic server to `client` which will then retransmit to `TCPForwarder`.

#### Customized Plugins
All plugins must implement the `IPlugin` interface. All network traffics will occur between `client` and `target`. Methods of all plugins will be called on these traffic. With plugins, developers can easily set up their own analysis tools.

As mentioned in [Handle SSL connection](#handle_ssl_connection), all traffic between `client` and `target` are in plain text. That's why all plugins are used in now.
