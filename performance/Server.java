import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.net.*;
import java.util.*;

public class Server {
  public static void main(String args[]) {
    int port = Integer.parseInt(args[0]);
    try {
      ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
      serverSocketChannel.configureBlocking(false);
      serverSocketChannel.socket().bind(new InetSocketAddress(port));
      System.out.println("Server listening on  : " + port );
      Selector selector = Selector.open();
      serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
      while(true) {
        selector.select();
        Iterator<SelectionKey> it = selector.selectedKeys().iterator();
        while(it.hasNext()) {
          SelectionKey key = it.next();
          it.remove();
          if(key.isAcceptable()) {
            System.out.println("One client connected");
            SocketChannel sc = serverSocketChannel.accept();
            new Thread(new ServerRunnable(sc)).start();
          }
        }
      }
    } catch(IOException e) {
      e.printStackTrace();
    }
  }
}

class ServerRunnable implements Runnable {
  private SocketChannel serverChannel;
  private final int BUF_SIZE = 2048;
  private final String UPLOAD = "UPLOAD", DOWNLOAD = "DOWNLOAD", QUIT = "QUIT";
  private Charset cs = StandardCharsets.UTF_8;
  public ServerRunnable(SocketChannel sc) {
    serverChannel = sc;
  }

  @Override
  public void run() {
    int num = 0;
    try {
      ByteBuffer buf = ByteBuffer.allocate(BUF_SIZE);
      boolean waitingCmd = true;
      while(true) {
        if(serverChannel.read((ByteBuffer)buf.clear()) > 0) {
          String cmd = cs.decode((ByteBuffer)buf.flip()).toString();
          System.out.println("" + num + " : " + cmd);
          num += 1;
          switch(cmd.split("::")[0]) {
            case UPLOAD : handleUpload(cmd.split("::")[1], Integer.parseInt(cmd.split("::")[2])); break;
            case DOWNLOAD : handleDownload(cmd.split("::")[1]); break;
            case QUIT : serverChannel.close(); return;
            default: break;
          }
        }
      }
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  private void handleUpload(String filename, int length) throws IOException {
    serverChannel.write(cs.encode("UPLOAD::" + filename));
    ByteBuffer buf = ByteBuffer.allocate(BUF_SIZE);
    FileChannel fc = new FileOutputStream(new File(filename)).getChannel();
    int len = 0;
    while(length > 0 && (len = serverChannel.read((ByteBuffer)buf.clear())) > 0) {
      length -= len;
      //assert(length >= 0);
      //System.out.println("1 : " + length);
      fc.write((ByteBuffer)buf.flip());
      //System.out.println("2 : " + length);
    }
    serverChannel.write(cs.encode("DONE::" + filename));
    //System.out.println(length);
    fc.close();
    System.out.println(length);
  }

  private void handleDownload(String filename) throws IOException {
    File f = new File(filename);
    serverChannel.write(cs.encode("DOWNLOAD::" + filename + "::" + f.length()));
    ByteBuffer buf = ByteBuffer.allocate(BUF_SIZE);
    FileChannel fc = new FileInputStream(f).getChannel();
    serverChannel.read((ByteBuffer)buf.clear());
    assert(cs.decode((ByteBuffer)buf.flip()).toString() == "DOWNLOAD::" + filename + "::" + f.length());
    while(fc.read((ByteBuffer)buf.clear()) > 0) {
      serverChannel.write((ByteBuffer)buf.flip());
    }
    fc.close();
  }
}
